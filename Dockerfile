# Sets up a build environment for the Solstice Pay.
# This image can produce builds for Linux and Android.
FROM cirrusci/flutter:2.10.1

SHELL ["/bin/bash", "-c"]
RUN apt update -y
RUN apt install -y cmake \
             ninja-build \
             clang \
             pkgconf \
             libgtk-3-dev \
             libclang-dev \
             llvm \
             android-sdk
ENV ANDROID_NDK_HOME /opt/android-ndk
ENV ANDROID_NDK_VERSION r22
RUN apt-get update -qq && \
    apt-get clean
RUN mkdir /opt/android-ndk-tmp && \
    cd /opt/android-ndk-tmp && \
    wget -q https://dl.google.com/android/repository/android-ndk-${ANDROID_NDK_VERSION}-linux-x86_64.zip && \
    unzip -q android-ndk-${ANDROID_NDK_VERSION}-linux-x86_64.zip && \
    mv ./android-ndk-${ANDROID_NDK_VERSION} ${ANDROID_NDK_HOME} && \
    cd ${ANDROID_NDK_HOME} && \
    rm -rf /opt/android-ndk-tmp

ENV PATH ${PATH}:${ANDROID_NDK_HOME}

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
RUN source $HOME/.cargo/env && cargo install cbindgen cargo-ndk just flutter_rust_bridge_codegen
RUN source $HOME/.cargo/env && rustup target add aarch64-apple-ios x86_64-apple-ios && \
	rustup target add aarch64-apple-darwin x86_64-apple-darwin && \
    rustup target add aarch64-linux-android armv7-linux-androideabi i686-linux-android x86_64-linux-android
RUN mkdir ~/.gradle
RUN dart pub global activate ffigen

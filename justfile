# Homebrew installs LLVM in a place that is not visible to ffigen.
# This explicitly specifies the place where the LLVM dylibs are kept.
llvm_path := if os() == "macos" {
    if arch() == "aarch64" {
        "--llvm-path /usr/local/homebrew/opt/llvm/"
    } else {
        "--llvm-path /opt/homebrew/opt/llvm"
    }
} else {
    ""
}

current_path := justfile_directory()

default: gen lint

gen:
    cd / && flutter_rust_bridge_codegen {{llvm_path}} \
        --rust-input {{current_path}}/native/src/api.rs \
        --dart-output {{current_path}}/lib/bridge_generated.dart \
        --c-output {{current_path}}/ios/Runner/bridge_generated.h
    cd {{current_path}}
    cp ios/Runner/bridge_generated.h macos/Runner/bridge_generated.h

lint:
    cd native && cargo fmt
    dart format .

clean:
    flutter clean
    cd native && cargo clean

# vim:expandtab:sw=4:ts=4

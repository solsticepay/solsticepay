import 'package:flutter_test/flutter_test.dart';

// ignore: avoid_relative_lib_imports
import '../lib/deep_link_routes.dart';

void main() {
  test('Counter value should be incremented', () {
    assert(isValidParams({'a'}, {'a'}));
    assert(!isValidParams({'a'}, {'b'}));
    assert(!isValidParams({'a'}, ['a', 'a']));
  });
}

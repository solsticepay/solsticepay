import 'package:flutter/material.dart';
import 'profile_hero.dart' show PhotoHero;
import '../contacts_data.dart' show ContactsData;
import '../image_db.dart' show ImageDb;
import '../image_picker.dart' show ImagePickerWidget;

class ContactPage extends StatefulWidget {
  final FileImage? image;
  final String address;
  final String name;

  const ContactPage(
      {required this.name,
      required this.address,
      required this.image,
      Key? key})
      : super(key: key);
  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  String? _chosenImagePath;
  ImageProvider<Object>? _chosenImage;

  void _onImageUpdate(String? path, ImageProvider<Object>? image) {
    setState(() {
      _chosenImagePath = path;
      _chosenImage = image;
    });
    if (_chosenImagePath != null) {
      ImageDb.saveProfileImageForContact(
          _chosenImagePath!, this.widget.address);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(this.widget.name),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Spacer(flex: 1),
          Center(
            child: PhotoHero(
              image: this._chosenImage ?? this.widget.image,
              radius: 150.0,
              onTap: () {},
            ),
          ),
          const Spacer(flex: 1),
          Container(
            constraints: const BoxConstraints(maxWidth: 200),
            child: ImagePickerWidget(_onImageUpdate),
          ),
          const Spacer(flex: 1),
          Text(this.widget.address, style: const TextStyle(fontSize: 20)),
          const Spacer(flex: 1),
          ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
            ),
            child: const Text('Delete'),
            onPressed: () {
              ContactsData.removeContact(this.widget.name).then((_) {
                Navigator.pop(context);
              });
            },
          ),
          const Spacer(flex: 20),
        ],
      ),
    );
  }
}

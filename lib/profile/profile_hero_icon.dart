import 'package:flutter/material.dart';
import 'profile_hero.dart' show PhotoHero;
import 'main_profile_page.dart' show MainProfilePage;

class ProfileHeroIcon extends StatelessWidget {
  final FileImage? image;

  const ProfileHeroIcon(this.image, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PhotoHero(
      image: this.image,
      radius: 75.0,
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute<void>(builder: (BuildContext context) {
          return MainProfilePage(image: this.image);
        }));
      },
    );
  }
}

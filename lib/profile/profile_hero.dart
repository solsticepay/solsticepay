import 'package:flutter/material.dart';

class PhotoHero extends StatelessWidget {
  const PhotoHero({
    Key? key,
    required this.image,
    required this.onTap,
    required this.radius,
  }) : super(key: key);

  final ImageProvider<Object>? image;
  final VoidCallback onTap;
  final double radius;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: radius,
      child: Hero(
        tag: 'main-profile-hero-image',
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: onTap,
            child: SizedBox(
              height: radius,
              child: CircleAvatar(
                backgroundImage: this.image,
                backgroundColor: const Color.fromARGB(47, 127, 127, 127),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

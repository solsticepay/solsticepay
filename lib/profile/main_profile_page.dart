import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart' show FlutterShare;
import '../button_components.dart' show SolsticeButtonProperties;
import '../settings/settings_data.dart' show SettingsData, FullName;
import 'profile_hero.dart' show PhotoHero;
import '../private_key_management/private_key_manager.dart'
    show PrivateKeyManager;
import '../solsticepay_links.dart' show getAddContactLink;
import '../image_db.dart' show ImageDb;
import '../image_picker.dart' show ImagePickerWidget;

class MainProfilePage extends StatefulWidget {
  final FileImage? image;

  const MainProfilePage({required this.image, Key? key}) : super(key: key);

  @override
  _MainProfilePageState createState() => _MainProfilePageState();
}

class _MainProfilePageState extends State<MainProfilePage> {
  String? _chosenImagePath;
  ImageProvider<Object>? _chosenImage;

  void _onImageUpdate(String? path, ImageProvider<Object>? image) {
    setState(() {
      _chosenImagePath = path;
      _chosenImage = image;
    });
    if (_chosenImagePath != null) {
      ImageDb.saveMainProfileImage(_chosenImagePath!);
    }
  }

  void share() {
    PrivateKeyManager.getPublicKey().then(
      (key) => SettingsData.getFullName().then(
        (fullName) => FlutterShare.share(
          title: 'Add Contact',
          text:
              '${fullName.toString()} would like you to add them as a contact on Solstice Pay.',
          linkUrl: getAddContactLink(key!, fullName.toString()),
          chooserTitle: '',
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: FutureBuilder<FullName>(
          future: SettingsData.getFullName(),
          builder: (
            BuildContext context,
            AsyncSnapshot<FullName> snapshot,
          ) {
            String text = 'Your profile';
            if (snapshot.hasData) {
              text = snapshot.data!.toString();
            }
            return Text(text);
          },
        ),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Spacer(flex: 1),
          Center(
            child: PhotoHero(
              image: this._chosenImage ?? this.widget.image,
              radius: 150.0,
              onTap: () {},
            ),
          ),
          const Spacer(flex: 1),
          Container(
            constraints: const BoxConstraints(maxWidth: 200),
            child: ImagePickerWidget(_onImageUpdate),
          ),
          const Spacer(flex: 1),
          FutureBuilder<String?>(
              future: PrivateKeyManager.getPublicKey(),
              builder: (
                BuildContext context,
                AsyncSnapshot<String?> snapshot,
              ) {
                if (snapshot.hasData) {
                  return Text(snapshot.data!,
                      style: const TextStyle(fontSize: 20));
                } else {
                  return const SizedBox.shrink();
                }
              }),
          const Spacer(flex: 1),
          ElevatedButton(
            style: SolsticeButtonProperties.buttonStyle,
            child: const Text('Share'),
            onPressed: share,
          ),
          const Spacer(flex: 20),
        ],
      ),
    );
  }
}

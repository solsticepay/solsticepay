import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart'
    show ImageSource, ImagePicker, XFile;
import 'package:file_picker_cross/file_picker_cross.dart'
    show FilePickerCross, FileTypeCross;

class ImagePickerWidget extends StatefulWidget {
  final void Function(String?, ImageProvider<Object>?) onImageUpdate;

  const ImagePickerWidget(this.onImageUpdate, {Key? key}) : super(key: key);

  @override
  _ImagePickerWidgetState createState() => _ImagePickerWidgetState();
}

class _ImagePickerWidgetState extends State<ImagePickerWidget> {
  final ImagePicker _picker = ImagePicker();

  ImageProvider<Object>? _getImageProvider(String? imageFilePath) {
    if (imageFilePath == null) {
      return null;
    }
    return (kIsWeb
        ? NetworkImage(imageFilePath)
        : FileImage(File(imageFilePath))) as ImageProvider<Object>;
  }

  Future<void> _onImageButtonPressed(ImageSource source) async {
    if (Platform.isAndroid || Platform.isIOS) {
      _onImageButtonPressedMobile(source);
    } else {
      _onImageButtonPressedDesktop(source);
    }
  }

  Future<void> _onImageButtonPressedDesktop(ImageSource source) async {
    String? imageFilePath;

    if (source == ImageSource.gallery) {
      try {
        final FilePickerCross pickedFile =
            await FilePickerCross.importFromStorage(
          type: FileTypeCross.custom,
          fileExtension: 'png,jpg,jpeg,webp',
        );

        setState(() {
          imageFilePath = pickedFile.path;
        });
      } catch (_) {
        setState(() {
          imageFilePath = null;
        });
      }
    } else {
      throw Exception('Camera is unimplemented for desktop platforms');
    }
    this.widget.onImageUpdate(imageFilePath, _getImageProvider(imageFilePath));
  }

  Future<void> _onImageButtonPressedMobile(ImageSource source) async {
    String? imageFilePath;

    try {
      final XFile? pickedFile = await _picker.pickImage(source: source);
      setState(() {
        imageFilePath = pickedFile?.path;
      });
    } catch (_) {
      setState(() {
        imageFilePath = null;
      });
    }
    this.widget.onImageUpdate(imageFilePath, _getImageProvider(imageFilePath));
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        ElevatedButton(
          onPressed: () {
            _onImageButtonPressed(ImageSource.gallery);
          },
          child: const Icon(Icons.photo),
        ),
        ElevatedButton(
          onPressed: (Platform.isAndroid || Platform.isIOS)
              ? () {
                  _onImageButtonPressed(ImageSource.camera);
                }
              : null,
          child: const Icon(Icons.camera_alt),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import '../private_key_management/private_key_manager.dart'
    show PrivateKeyManager;
import 'components.dart' show HintText;
import 'name_entry.dart' show navigateToNameEntry;
import 'setup_finished.dart' show navigateToSetupFinished;

void navigateToChoosePassword(BuildContext context,
    {String? importedPrivateKey}) {
  Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (context) =>
          _ChoosePassword(importedPrivateKey: importedPrivateKey)));
}

class _ChoosePassword extends StatefulWidget {
  final String? importedPrivateKey;
  const _ChoosePassword({this.importedPrivateKey, Key? key}) : super(key: key);
  @override
  _ChoosePasswordState createState() => _ChoosePasswordState();
}

class _ChoosePasswordState extends State<_ChoosePassword> {
  final passwordTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(60.0),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Spacer(),
              const Text('Keep your funds safe',
                  style: TextStyle(fontSize: 40.0)),
              const SizedBox(width: 0, height: 100.0),
              TextFormField(
                style: const TextStyle(fontSize: 25.0),
                controller: passwordTextController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Password',
                ),
                obscureText: true,
              ),
              const SizedBox(width: 0, height: 50.0),
              ElevatedButton(
                child: const Text(
                  'Continue',
                  style: TextStyle(fontSize: 25.0),
                ),
                onPressed: () {
                  final password = passwordTextController.text;
                  // Note - these PrivateKeyManager APIs are async and occur in
                  // the background while the user continues to finish initial
                  // setup
                  if (widget.importedPrivateKey == null) {
                    PrivateKeyManager.generateAndSaveKey(password);
                    navigateToSetupFinished(context);
                  } else {
                    PrivateKeyManager.addPrivateKey(
                      widget.importedPrivateKey!,
                      password,
                    );
                    navigateToNameEntry(context);
                  }
                },
              ),
              const Spacer(),
              const HintText(
                'Don\'t forget to add it to your password manager! Nobody can recover your money if you forget it.',
              ),
              const SizedBox(width: 0, height: 10.0),
            ],
          ),
        ),
      ),
    );
  }
}

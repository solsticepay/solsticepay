import 'package:flutter/material.dart';
import 'name_entry.dart' show navigateToNameEntry;
import 'wallet_import.dart' show navigateToWalletImport;

// TODO resume initial setup where it was left off if a user leaves the app and
// restarts it again
class InitialSetupScreen extends StatelessWidget {
  const InitialSetupScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Spacer(),
          const SizedBox(width: 0, height: 180.0),
          const Text('Welcome to Solstice Pay',
              style: TextStyle(fontSize: 40.0)),
          const SizedBox(width: 0, height: 120.0),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              minimumSize: const Size(180.0, 80.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
            ),
            child: const Text(
              'Get started',
              style: TextStyle(fontSize: 30.0),
            ),
            onPressed: () {
              navigateToNameEntry(context);
            },
          ),
          const Spacer(),
          TextButton(
            style: ElevatedButton.styleFrom(
              minimumSize: const Size(180.0, 80.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
            ),
            child: const Text(
              'I have an existing wallet to import',
              style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.normal),
            ),
            onPressed: () {
              navigateToWalletImport(context);
            },
          ),
          const SizedBox(width: 0, height: 60.0),
        ],
      ),
    ));
  }
}

import 'package:flutter/material.dart';
import 'choose_password.dart' show navigateToChoosePassword;

void navigateToImportPrivateKey(BuildContext context) {
  Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (context) => const _ImportPrivateKey()));
}

class _ImportPrivateKey extends StatefulWidget {
  const _ImportPrivateKey({Key? key}) : super(key: key);
  @override
  _ImportPrivateKeyState createState() => _ImportPrivateKeyState();
}

class _ImportPrivateKeyState extends State<_ImportPrivateKey> {
  final privateKeyTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(60.0),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Spacer(),
              const Text('Import private key',
                  style: TextStyle(fontSize: 40.0)),
              const SizedBox(width: 0, height: 100.0),
              TextFormField(
                style: const TextStyle(fontSize: 25.0),
                controller: privateKeyTextController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Private key',
                ),
                obscureText: true,
              ),
              const SizedBox(width: 0, height: 50.0),
              ElevatedButton(
                child: const Text(
                  'Continue',
                  style: TextStyle(fontSize: 25.0),
                ),
                // TODO disable button if key is invalid
                onPressed: () {
                  final privateKey = privateKeyTextController.text;
                  navigateToChoosePassword(context,
                      importedPrivateKey: privateKey);
                },
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}

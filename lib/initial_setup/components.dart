import 'package:flutter/material.dart';

class HintText extends StatelessWidget {
  final String text;

  const HintText(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      this.text,
      textAlign: TextAlign.center,
      style: TextStyle(
          fontSize: 20.0,
          color: Theme.of(context).colorScheme.onBackground.withOpacity(0.4),
          height: 1.5),
    );
  }
}

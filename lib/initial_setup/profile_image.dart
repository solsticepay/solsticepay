import 'package:flutter/material.dart';
import '../private_key_management/private_key_manager.dart'
    show PrivateKeyManager;
import 'choose_password.dart' show navigateToChoosePassword;
import 'setup_finished.dart' show navigateToSetupFinished;
import '../image_picker.dart' show ImagePickerWidget;
import '../image_db.dart' show ImageDb;

void navigateToProfileImage(BuildContext context) {
  Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (context) => const _ProfileImage()));
}

class _ProfileImage extends StatefulWidget {
  const _ProfileImage({Key? key}) : super(key: key);
  @override
  _ProfileImageState createState() => _ProfileImageState();
}

class _ProfileImageState extends State<_ProfileImage> {
  String? _chosenImagePath;
  ImageProvider<Object>? _chosenImage;

  void _onImageUpdate(String? path, ImageProvider<Object>? image) {
    setState(() {
      _chosenImagePath = path;
      _chosenImage = image;
    });
  }

  void _continueToNextScreen(BuildContext context) {
    PrivateKeyManager.getPublicKey().then((key) {
      if (key == null) {
        // Choose a password and generate key data if it
        // doesn't exist already
        navigateToChoosePassword(context);
      } else {
        // if private key data already exists, finish setup
        // immediately.
        navigateToSetupFinished(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(60.0),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Spacer(),
              const Text('Let\'s add your picture, too',
                  style: TextStyle(fontSize: 40.0)),
              const SizedBox(width: 0, height: 40.0),
              Expanded(
                flex: 16,
                child: Container(
                  constraints: const BoxConstraints(maxWidth: 450),
                  child: Column(children: <Widget>[
                    Expanded(
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: (_chosenImagePath != null)
                            ? CircleAvatar(
                                backgroundColor:
                                    const Color.fromARGB(47, 127, 127, 127),
                                backgroundImage: _chosenImage,
                              )
                            : const CircleAvatar(
                                backgroundColor:
                                    Color.fromARGB(47, 127, 127, 127),
                                child: Text(
                                  'Only you and your contacts\nwill see your image.',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color.fromARGB(255, 120, 120, 120),
                                    fontFamily: 'Ubuntu',
                                  ),
                                ),
                              ),
                      ),
                    ),
                    ImagePickerWidget(_onImageUpdate),
                  ]),
                ),
              ),
              const SizedBox(width: 0, height: 40.0),
              ElevatedButton(
                child: const Text(
                  'Use this image!',
                  style: TextStyle(fontSize: 30.0),
                ),
                onPressed: (_chosenImage == null || _chosenImagePath == null)
                    ? null
                    : () {
                        // Async, but occurs in the background. It should
                        // finish by the time initial setup is complete.
                        ImageDb.saveMainProfileImage(_chosenImagePath!);
                        _continueToNextScreen(context);
                      },
              ),
              const SizedBox(width: 0, height: 30.0),
              TextButton(
                child: const Text(
                  'No thanks, I\'ll use a random image instead',
                  style:
                      TextStyle(fontSize: 20.0, fontWeight: FontWeight.normal),
                ),
                onPressed: () {
                  _continueToNextScreen(context);
                },
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}

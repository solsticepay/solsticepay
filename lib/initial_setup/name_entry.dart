import 'package:flutter/material.dart';
import 'components.dart' show HintText;
import '../settings/settings_data.dart' show SettingsData, FullName;
import 'profile_image.dart' show navigateToProfileImage;

void navigateToNameEntry(BuildContext context) {
  Navigator.of(context)
      .push(MaterialPageRoute<void>(builder: (context) => const _NameEntry()));
}

class _NameEntry extends StatefulWidget {
  const _NameEntry({Key? key}) : super(key: key);
  @override
  _NameEntryState createState() => _NameEntryState();
}

class _NameEntryState extends State<_NameEntry> {
  final firstNameTextController = TextEditingController();
  final lastNameTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    firstNameTextController.addListener(() {
      setState(() {});
    });
    lastNameTextController.addListener(() {
      setState(() {});
    });
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(60.0),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Spacer(),
              const Text('What\'s your name?',
                  style: TextStyle(fontSize: 40.0)),
              const SizedBox(width: 0, height: 100.0),
              TextFormField(
                style: const TextStyle(fontSize: 25.0),
                controller: firstNameTextController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'First name',
                ),
              ),
              const SizedBox(width: 0, height: 20.0),
              TextFormField(
                style: const TextStyle(fontSize: 25.0),
                controller: lastNameTextController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Last name',
                ),
              ),
              const SizedBox(width: 0, height: 50.0),
              ElevatedButton(
                child: const Text(
                  'Continue',
                  style: TextStyle(fontSize: 25.0),
                ),
                onPressed: (firstNameTextController.text != '' &&
                        lastNameTextController.text != '')
                    ? () {
                        SettingsData.setFullName(FullName(
                            firstNameTextController.text,
                            lastNameTextController.text));
                        navigateToProfileImage(context);
                      }
                    : null,
              ),
              const Spacer(),
              const HintText(
                'Feel free to use your nickname! Only you and your contacts will see it.',
              ),
              const SizedBox(width: 0, height: 10.0),
            ],
          ),
        ),
      ),
    );
  }
}

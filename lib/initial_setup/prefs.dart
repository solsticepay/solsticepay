import 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;

const _didInitialSetupKey = 'completed_initial_setup';

Future<bool> didInitialSetup() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool(_didInitialSetupKey) ?? false;
}

Future<void> markInitialSetupAsCompleted() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool(_didInitialSetupKey, true);
}

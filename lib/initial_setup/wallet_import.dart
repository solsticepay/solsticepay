import 'package:flutter/material.dart';
import 'import_private_key.dart' show navigateToImportPrivateKey;

void navigateToWalletImport(BuildContext context) {
  Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (context) => const _WalletImport()));
}

class _WalletImport extends StatefulWidget {
  const _WalletImport({Key? key}) : super(key: key);
  @override
  _WalletImportState createState() => _WalletImportState();
}

class _WalletImportState extends State<_WalletImport> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(60.0),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Spacer(),
              const Text('Not your first time?',
                  style: TextStyle(fontSize: 40.0)),
              const SizedBox(width: 0, height: 100.0),
              const Text(
                'You can import all your balances into Solstice Pay from a previous installation, or from any other Solana wallet application.',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20.0, height: 1.5),
              ),
              const SizedBox(width: 0, height: 70.0),
              const ElevatedButton(
                child: Text(
                  'Import seed phrase',
                  style: TextStyle(fontSize: 30.0),
                ),
                // TODO - support seed phrase import
                onPressed: null,
              ),
              const SizedBox(width: 0, height: 50.0),
              ElevatedButton(
                child: const Text(
                  'Import private key',
                  style: TextStyle(fontSize: 30.0),
                ),
                onPressed: () {
                  navigateToImportPrivateKey(context);
                },
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}

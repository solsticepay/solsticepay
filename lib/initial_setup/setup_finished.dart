import 'package:flutter/material.dart';
import '../home_page.dart' show MyHome;
import 'prefs.dart' show markInitialSetupAsCompleted;

void navigateToSetupFinished(BuildContext context) {
  Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (context) => const _SetupFinished()));
}

class _SetupFinished extends StatefulWidget {
  const _SetupFinished({Key? key}) : super(key: key);

  @override
  _SetupFinishedState createState() => _SetupFinishedState();
}

class _SetupFinishedState extends State<_SetupFinished> {
  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 2), () {
      markInitialSetupAsCompleted()
          .then((_) => Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute<void>(builder: (context) => const MyHome()),
                (Route<dynamic> route) => route is MyHome,
              ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(60.0),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[
              Spacer(),
              Text('You\'re all set!', style: TextStyle(fontSize: 50.0)),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}

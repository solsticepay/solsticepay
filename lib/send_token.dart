import 'package:flutter/material.dart';
import 'package:collection/collection.dart'; // firstWhereOrNull
import 'dart:async';
import 'ffi.dart' show rust;
import 'private_key_management/private_key_manager.dart' show PrivateKeyManager;

import 'contacts_data.dart' show Contact, ContactsData;
import 'send_confirmation.dart';
import 'price_indicator.dart'
    show
        PriceIndicator,
        PriceConversionText,
        solanaPriceFetcher,
        getAmountInSol,
        fetchSolanaPrice,
        getAmountInUsd;
import 'bridge_generated.dart' show SolanaPayRequestFFI;

class SendScreen extends StatefulWidget {
  final SolanaPayRequestFFI? request;

  const SendScreen({this.request, Key? key}) : super(key: key);
  @override
  SendScreenState createState() => SendScreenState();
}

class SendScreenState extends State<SendScreen> {
  final recipientTextController = TextEditingController();

  final amountUSDTextController = TextEditingController();
  double? solPrice;
  late Timer _timerForInterval;
  bool _initialSet = false;
  Contact? selectedContact = null;

  SendScreenState() {
    _timerForInterval = solanaPriceFetcher(
      'usd',
      const Duration(seconds: 30),
      (price) => setState(() => this.solPrice = price),
    );
  }

  @override
  void initState() {
    super.initState();
    recipientTextController.addListener(_updateContactDropdown);
  }

  @override
  void dispose() {
    _timerForInterval.cancel();
    recipientTextController.dispose();
    super.dispose();
  }

  /// Ensure the value of the contact dropdown is consistent with the value of
  /// the recipient address text field.
  void _updateContactDropdown() {
    ContactsData.getAllContacts().then((contacts) {
      final matchingContact = contacts.firstWhereOrNull(
          (contact) => contact.address == recipientTextController.text);
      setState(() {
        selectedContact = matchingContact;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!_initialSet) {
      if (widget.request != null) {
        recipientTextController.text = widget.request!.recipient;
        fetchSolanaPrice('usd').then((price) {
          amountUSDTextController.text =
              getAmountInUsd(widget.request!.amount!, price).toString();
        });
        amountUSDTextController.text = '0.00';
      }
      _initialSet = true;
    }
    amountUSDTextController.addListener(() {
      setState(() {});
    });
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sending SOL'),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Spacer(),
              PriceIndicator(solPrice: solPrice),
              const Spacer(),
              FutureBuilder<Iterable<Contact>>(
                future: ContactsData.getAllContacts(),
                builder: (
                  BuildContext context,
                  AsyncSnapshot<Iterable<Contact>> snapshot,
                ) {
                  if (snapshot.hasData) {
                    return DropdownButton<Contact?>(
                      value: selectedContact,
                      onChanged: (Contact? newValue) {
                        final addressText = newValue?.address ?? '';
                        setState(() {
                          recipientTextController.text = addressText;
                          selectedContact = newValue;
                        });
                      },
                      items: snapshot.data!
                          .map<DropdownMenuItem<Contact>>((Contact value) {
                        return DropdownMenuItem<Contact>(
                          value: value,
                          child: Text(value.name),
                        );
                      }).toList(),
                      hint: const Text('Contacts...'),
                    );
                  }
                  return const Text('Loading contacts...');
                },
              ),
              TextFormField(
                controller: recipientTextController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Recipient address',
                ),
              ),
              TextFormField(
                controller: amountUSDTextController,
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Amount USD',
                ),
              ),
              PriceConversionText(
                solPrice: solPrice,
                value: amountUSDTextController.text,
              ),
              ElevatedButton(
                onPressed: () {
                  _navigateToConfirmationScreen(context);
                },
                child: const Text(
                  'Continue...',
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }

  void _navigateToConfirmationScreen(BuildContext context) async {
    if (this.solPrice == null) {
      return;
    }
    double? amount =
        getAmountInSol(this.amountUSDTextController.text, this.solPrice!);
    if (amount == null) {
      return;
    }

    final recipient = recipientTextController.text;

    final publicKey = await PrivateKeyManager.getPublicKey();
    if (publicKey == null) {
      throw Exception("Can't find public key");
    }
    final preparedTransaction = await rust.createTransaction(
        senderBase58Pubkey: publicKey,
        amount: amount,
        recipientBase58Pubkey: recipient);

    final success = await Navigator.of(context).push(MaterialPageRoute<bool>(
        builder: (context) => SendConfirmationScreen(
              preparedTransaction.messageJson,
              recipient,
              amount,
              preparedTransaction.fee,
            )));

    if (success == null) {
      return;
    } else if (success) {
      Navigator.pop(context);
    }
  }
}

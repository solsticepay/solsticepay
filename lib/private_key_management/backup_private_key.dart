import 'dart:io' show Platform;
import 'dart:convert' show utf8;
import 'dart:typed_data' show Uint8List;

import 'package:flutter_file_dialog/flutter_file_dialog.dart'
    show SaveFileDialogParams, FlutterFileDialog;
import 'package:flutter/services.dart' show Clipboard, ClipboardData;
import 'package:flutter/material.dart';

import 'private_key_manager.dart';
import 'ask_user_to_decrypt_key.dart';

// TODO: Add ability in UI to getPrivateKey
// ignore: unused_element
void _getPrivateKey(
  BuildContext context,
  String password,
) async {
  try {
    final decrypted = await PrivateKeyManager.getPrivateKey(password);
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(SnackBar(content: Text('private key: $decrypted')));
  } catch (_) {
    ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(const SnackBar(content: Text('Invalid password')));
  }
}

class BackupPrivateKey extends StatefulWidget {
  const BackupPrivateKey({Key? key}) : super(key: key);
  @override
  BackupPrivateKeyState createState() => BackupPrivateKeyState();
}

class BackupPrivateKeyState extends State {
  final privateKeyTextController = TextEditingController();
  final passwordKeyTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Backup data'),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const Text(
                  'Your balances in Solstice Pay are secured by a special "private key".',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 24)),
              const SizedBox(height: 40),
              const Text(
                  'Please save your private key in a safe place that only you can access.',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 24)),
              const SizedBox(height: 40),
              const Text(
                  'It is the only way to recover your money if you lose your device.',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 32)),
              const SizedBox(height: 60),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  ElevatedButton(
                    child: const Text('Backup private key'),
                    onPressed: () {
                      askUserToDecrypt(
                        context,
                        'Do not share your private key with ANYONE!\n'
                        'They will be able to withdraw your money out of this account without your knowledge - forever.\n\n'
                        'Enter your password to continue.',
                      ).then((privateKey) {
                        // TODO make private key available for download or clipboard copy
                        if (Platform.isAndroid || Platform.isIOS) {
                          savePrivateKeyToFile(privateKey!);
                        } else {
                          savePrivateKeyToClipboard(privateKey!, context);
                        }
                      });
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void savePrivateKeyToClipboard(String privateKey, BuildContext context) {
  Clipboard.setData(ClipboardData(text: privateKey));
  ScaffoldMessenger.of(context)
    ..removeCurrentSnackBar()
    ..showSnackBar(
      const SnackBar(
        content: Text('Your private key has been copied to the clipboard.'),
      ),
    );
}

void savePrivateKeyToFile(String privateKey) async {
  final List<int> list = utf8.encode(privateKey);
  final Uint8List bytes = Uint8List.fromList(list);

  final params = SaveFileDialogParams(
      fileName: 'Solstice Pay private key backup.txt', data: bytes);
  await FlutterFileDialog.saveFile(params: params);
}

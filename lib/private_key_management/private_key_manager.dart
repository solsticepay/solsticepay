import 'package:shared_preferences/shared_preferences.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:dbcrypt/dbcrypt.dart';
import 'dart:convert';
import '../ffi.dart' show rust;

/*
TODO: Migrate to https://pub.dev/packages/encryptions
*/

class SolanaKeyData {
  static const publicKeyKey = 'public_key';
  static const privateKeyHashedKey = 'private_key_hashed';
  static const saltKey = 'salt';
  final String publicKey;
  final String privateKeyHashed;
  final String salt;

  SolanaKeyData(this.publicKey, this.privateKeyHashed, this.salt);

  // This should only ever be passed a map directly from the `solana_key_data`
  // field in shared preferences, so each of the three keys are assumed to be
  // set.
  SolanaKeyData.fromJson(Map<String, dynamic> json)
      : publicKey = json[SolanaKeyData.publicKeyKey]! as String,
        privateKeyHashed = json[SolanaKeyData.privateKeyHashedKey]! as String,
        salt = json[SolanaKeyData.saltKey]! as String;

  Map<String, dynamic> toJson() {
    return {
      SolanaKeyData.publicKeyKey: this.publicKey,
      SolanaKeyData.privateKeyHashedKey: this.privateKeyHashed,
      SolanaKeyData.saltKey: this.salt,
    };
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other is SolanaKeyData &&
            other.publicKey == this.publicKey &&
            other.privateKeyHashed == this.privateKeyHashed &&
            other.salt == this.salt);
  }

  @override
  int get hashCode => Object.hash(publicKey, privateKeyHashed, salt);
}

class PrivateKeyManager {
  static const sharedPreferencesKey = 'solana_key_data';

  static Future<SolanaKeyData?> getSolanaKeyData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final dataString = prefs.getString(PrivateKeyManager.sharedPreferencesKey);
    if (dataString == null) {
      return null;
    }

    final keyData =
        SolanaKeyData.fromJson(jsonDecode(dataString) as Map<String, dynamic>);

    return keyData;
  }

  static Future<String?> getPublicKey() async {
    final keyData = await PrivateKeyManager.getSolanaKeyData();
    return keyData?.publicKey;
  }

  // TODO - only should be used for full app settings resets
  static Future<void> removePrivateKey() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(
      PrivateKeyManager.sharedPreferencesKey,
    );
  }

  static Future<void> _setSolanaKeyData(SolanaKeyData data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(
      PrivateKeyManager.sharedPreferencesKey,
      jsonEncode(data),
    );
  }

  static Future<void> generateAndSaveKey(
    String password,
  ) async {
    final privateKey = await rust.generateNewPrivateKey();
    await addPrivateKey(privateKey, password);
  }

  static Future<void> addPrivateKey(
    String privateKey,
    String password,
  ) async {
    final salt = DBCrypt().gensalt();
    var hashedPassword = DBCrypt()
        .hashpw(
          password,
          salt,
        )
        .substring(
          0,
          32,
        );

    final key = encrypt.Key.fromUtf8(hashedPassword);
    final iv = encrypt.IV.fromLength(16);

    final encrypter = encrypt.Encrypter(encrypt.AES(key));
    final encrypted = encrypter
        .encrypt(
          privateKey,
          iv: iv,
        )
        .base64;
    final publicKey = await rust.privateKeyToPubkey(privateKey: privateKey);
    final solanaKeyData = SolanaKeyData(publicKey, encrypted, salt);
    await _setSolanaKeyData(solanaKeyData);
  }

  static Future<String?> getPrivateKey(
    String password,
  ) async {
    final keyData = await PrivateKeyManager.getSolanaKeyData();
    if (keyData == null) {
      return null;
    }
    String storedPk = keyData.privateKeyHashed;
    String salt = keyData.salt;
    final iv = encrypt.IV.fromLength(16);

    var hashedPassword = DBCrypt().hashpw(password, salt).substring(0, 32);

    final key = encrypt.Key.fromUtf8(hashedPassword);
    final encrypter = encrypt.Encrypter(encrypt.AES(key));
    try {
      final decrypted = encrypter.decrypt(
        encrypt.Encrypted.fromBase64(storedPk),
        iv: iv,
      );
      return decrypted;
    } catch (_) {
      return null;
    }
  }
}

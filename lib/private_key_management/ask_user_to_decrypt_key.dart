import 'package:flutter/material.dart';
import 'private_key_manager.dart';

Future<String?> askUserToDecrypt(
  BuildContext context,
  String title,
) async {
  final passwordKeyTextController = TextEditingController();
  final Future<String?> dialog = showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          scrollable: true,
          content: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                controller: passwordKeyTextController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Password',
                ),
                obscureText: true,
              ),
              const SizedBox(height: 30),
              ElevatedButton(
                onPressed: () {
                  PrivateKeyManager.getPrivateKey(
                    passwordKeyTextController.text,
                  ).then((result) {
                    if (result != null) {
                      Navigator.of(context).pop(result);
                    } else {
                      ScaffoldMessenger.of(context)
                        ..removeCurrentSnackBar()
                        ..showSnackBar(
                            const SnackBar(content: Text('INVALID PASSWORD')));
                    }
                  });
                },
                child: const Text('Confirm'),
              ),
            ],
          ),
        );
      });
  return dialog;
}

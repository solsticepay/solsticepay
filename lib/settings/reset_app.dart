import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

void finalWarningToDeleteApp(BuildContext context) {
  showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('Last chance!'),
        content: SingleChildScrollView(
          child: ListBody(
            children: const <Widget>[
              Text('Please back up your private key before continuing'),
            ],
          ),
        ),
        actionsAlignment: MainAxisAlignment.spaceBetween,
        actions: <Widget>[
          const Spacer(),
          ElevatedButton(
            child: const Text(
              'back',
              style: TextStyle(fontSize: 20.0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          const Spacer(),
          TextButton(
            child: const Text(
              'Reset app and quit',
              style: TextStyle(
                fontSize: 14,
                color: Colors.red,
              ),
            ),
            onPressed: () {
              SharedPreferences.getInstance().then((preferences) {
                preferences.clear().then(
                  (value) {
                    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                  },
                );
              });
            },
          ),
          const Spacer(),
        ],
      );
    },
  );
}

void initialWarningToDeleteApp(BuildContext parentContext) {
  showDialog<void>(
    context: parentContext,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('Delete everything'),
        content: SingleChildScrollView(
          child: ListBody(
            children: const <Widget>[
              Text('WARNING - This will remove everything!'),
              Text(
                  'This may result in permanent loss of your wallet and money!'),
              Text('Please back up your keys before doing this.'),
            ],
          ),
        ),
        actionsAlignment: MainAxisAlignment.spaceBetween,
        actions: <Widget>[
          const Spacer(),
          ElevatedButton(
            child: const Text(
              'back',
              style: TextStyle(fontSize: 20.0),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          const Spacer(),
          TextButton(
            child: const Text(
              'Reset app',
              style: TextStyle(
                fontSize: 14,
                color: Colors.red,
              ),
            ),
            onPressed: () {
              Navigator.of(context).pop();
              finalWarningToDeleteApp(parentContext);
            },
          ),
          const Spacer(),
        ],
      );
    },
  );
}

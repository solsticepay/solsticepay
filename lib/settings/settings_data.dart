import 'package:flutter/material.dart' show ThemeMode;
import 'dart:convert' show json, jsonEncode;
import 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;

import '../themes.dart' show themeHandler;

class SettingsDataObject {
  static const useSolanaSchemeKey = 'use_solana_scheme';
  static const sharedPreferencesKey = 'settings';
  static const firstNameKey = 'firstName';
  static const lastNameKey = 'lastName';
  late bool useSolanaScheme;
  late String firstName;
  late String lastName;
  static const useDarkModeKey = 'use_dark_mode';
  late bool useDarkMode;

  SettingsDataObject.fromJson(Map<String, dynamic> json)
      : useSolanaScheme =
            (json[SettingsDataObject.useSolanaSchemeKey] ?? false) as bool,
        firstName = (json[SettingsDataObject.firstNameKey] ?? '') as String,
        lastName = (json[SettingsDataObject.lastNameKey] ?? '') as String,
        useDarkMode = (json[SettingsDataObject.useDarkModeKey] ??
            ThemeMode.system == ThemeMode.dark) as bool;

  Map<String, dynamic> toJson() {
    return {
      SettingsDataObject.useSolanaSchemeKey: this.useSolanaScheme,
      SettingsDataObject.firstNameKey: this.firstName,
      SettingsDataObject.lastNameKey: this.lastName,
      SettingsDataObject.useDarkModeKey: this.useDarkMode,
    };
  }
}

class FullName {
  final String firstName;
  final String lastName;

  FullName(this.firstName, this.lastName);

  @override
  String toString() {
    return '${this.firstName} ${this.lastName}';
  }
}

class SettingsData {
  static SettingsDataObject getSettingsDataObject(SharedPreferences prefs) {
    final shared = prefs.getString(SettingsDataObject.sharedPreferencesKey);
    if (shared == null) {
      return SettingsDataObject.fromJson({});
    } else {
      return SettingsDataObject.fromJson(
          json.decode(shared) as Map<String, dynamic>);
    }
  }

  static void setSettingsDataObject(SettingsDataObject settings) {
    SharedPreferences.getInstance().then((value) {
      value.setString(
        SettingsDataObject.sharedPreferencesKey,
        jsonEncode(settings.toJson()),
      );
    });
  }

  static void setUseSolanaScheme(bool useSolanaScheme) {
    SharedPreferences.getInstance().then((prefs) {
      SettingsDataObject settings = getSettingsDataObject(prefs);
      settings.useSolanaScheme = useSolanaScheme;
      setSettingsDataObject(settings);
    });
  }

  static Future<bool> getUseSolanaScheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return getSettingsDataObject(prefs).useSolanaScheme;
  }

  static Future<String> getFirstName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return getSettingsDataObject(prefs).firstName;
  }

  static Future<FullName> getFullName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final data = getSettingsDataObject(prefs);
    return FullName(data.firstName, data.lastName);
  }

  static Future<void> setFullName(FullName fullName) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final data = getSettingsDataObject(prefs);
    data.firstName = fullName.firstName;
    data.lastName = fullName.lastName;
    setSettingsDataObject(data);
  }

  static void setUseDarkMode(bool useDarkMode) {
    SharedPreferences.getInstance().then((prefs) {
      SettingsDataObject settings = getSettingsDataObject(prefs);
      settings.useDarkMode = useDarkMode;
      setSettingsDataObject(settings);
      themeHandler.setDarkMode(useDarkMode);
    });
  }

  static Future<bool> getUseDarkMode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return getSettingsDataObject(prefs).useDarkMode;
  }
}

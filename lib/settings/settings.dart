import 'package:flutter/foundation.dart' show kDebugMode;
import 'package:flutter/material.dart';
import '../private_key_management/backup_private_key.dart'
    show BackupPrivateKey;
import '../contacts_management.dart' show ContactsManagement;
import 'reset_app.dart' show initialWarningToDeleteApp;
import 'settings_data.dart' show SettingsData;

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);
  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          const Spacer(flex: 10),
          ElevatedButton(
            child: const Text(
              'Backup data',
              style: TextStyle(fontSize: 20.0),
            ),
            onPressed: () {
              _navigateToBackupPrivateKeyScreen(context);
            },
          ),
          const Spacer(flex: 1),
          ElevatedButton(
            child: const Text(
              'Contacts',
              style: TextStyle(fontSize: 20.0),
            ),
            onPressed: () {
              _navigateToContactsManagement(context);
            },
          ),
          const Spacer(flex: 1),
          kDebugMode
              ? ElevatedButton(
                  child: const Text(
                    'Reset App',
                    style: TextStyle(fontSize: 20.0, color: Colors.red),
                  ),
                  onPressed: () => initialWarningToDeleteApp(context),
                )
              : const SizedBox.shrink(),
          const Spacer(flex: 1),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Use Solana Pay scheme'),
              FutureBuilder<bool>(
                future: SettingsData.getUseSolanaScheme(),
                builder: (
                  BuildContext context,
                  AsyncSnapshot<bool> snapshot,
                ) {
                  bool switchState = false;
                  if (snapshot.hasData) {
                    switchState = snapshot.data!;
                  }
                  return Switch(
                    value: switchState,
                    onChanged: (value) {
                      setState(() {
                        SettingsData.setUseSolanaScheme(value);
                      });
                    },
                  );
                },
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('Dark Mode'),
              FutureBuilder<bool>(
                future: SettingsData.getUseDarkMode(),
                builder: (
                  BuildContext context,
                  AsyncSnapshot<bool> snapshot,
                ) {
                  bool switchState = false;
                  if (snapshot.hasData) {
                    switchState = snapshot.data!;
                  }
                  return Switch(
                    value: switchState,
                    onChanged: (value) {
                      setState(() {
                        SettingsData.setUseDarkMode(value);
                      });
                    },
                  );
                },
              ),
            ],
          ),
          const Spacer(flex: 10),
        ]),
      ),
    );
  }
}

void _navigateToBackupPrivateKeyScreen(BuildContext context) {
  Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (context) => const BackupPrivateKey()));
}

void _navigateToContactsManagement(BuildContext context) {
  Navigator.of(context).push(MaterialPageRoute<void>(
      builder: (context) => const ContactsManagement()));
}

import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart' show FlutterShare;
import 'dart:async';

import 'contacts_data.dart' show Contact;
import 'solsticepay_links.dart' show getPayUrl;
import 'private_key_management/private_key_manager.dart' show PrivateKeyManager;
import 'price_indicator.dart'
    show
        PriceIndicator,
        PriceConversionText,
        solanaPriceFetcher,
        getAmountInSol;

extension Ex on double {
  double toPrecision(int n) => double.parse(toStringAsFixed(n));
}

class RequestScreen extends StatefulWidget {
  const RequestScreen({Key? key}) : super(key: key);
  @override
  RequestScreenState createState() => RequestScreenState();
}

class RequestScreenState extends State<RequestScreen> {
  final amountUSDTextController = TextEditingController();
  final memoTextController = TextEditingController();
  double? solPrice;
  late Timer _timerForInterval;
  Contact? selectedContact = null;

  RequestScreenState() {
    _timerForInterval = solanaPriceFetcher(
      'usd',
      const Duration(seconds: 30),
      (price) => setState(() => this.solPrice = price),
    );
  }

  @override
  void dispose() {
    _timerForInterval.cancel();
    super.dispose();
  }

  Future<void> share() async {
    final publicKey = await PrivateKeyManager.getPublicKey();
    final usdAmount = amountUSDTextController.text;
    final solAmount = getAmountInSol(usdAmount, solPrice!);
    final message =
        memoTextController.text.isEmpty ? null : memoTextController.text;

    if (publicKey != null && solPrice != null && solAmount != null) {
      getPayUrl(publicKey, solAmount, message).then((linkUrl) {
        FlutterShare.share(
          title: 'Requesting \$$usdAmount',
          text:
              'Payment Request for $usdAmount USD / ${solAmount.toPrecision(6)} SOL\n\nMemo:\n${memoTextController.text}',
          linkUrl: linkUrl,
          chooserTitle: '',
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    amountUSDTextController.addListener(() {
      setState(() {});
    });
    return Scaffold(
      appBar: AppBar(
        title: const Text('Requesting SOL'),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Spacer(),
              PriceIndicator(solPrice: solPrice),
              const Spacer(),
              TextFormField(
                keyboardType: TextInputType.multiline,
                maxLines: 5,
                controller: memoTextController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Memo',
                ),
              ),
              const Spacer(),
              TextFormField(
                controller: amountUSDTextController,
                keyboardType:
                    const TextInputType.numberWithOptions(decimal: true),
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Amount USD',
                ),
              ),
              PriceConversionText(
                solPrice: solPrice,
                value: amountUSDTextController.text,
              ),
              ElevatedButton(
                onPressed: this.share,
                child: const Text(
                  'Request...',
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}

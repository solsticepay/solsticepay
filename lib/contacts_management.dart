import 'package:flutter/material.dart';

import 'image_db.dart' show ImageDb;
import 'contacts_data.dart' show ContactsData, Contact;
import 'profile/contact_page.dart' show ContactPage;

class AddContactIntent {
  final String contactName;
  final String contactAddress;
  AddContactIntent({required this.contactName, required this.contactAddress});
}

class ContactsManagement extends StatefulWidget {
  final AddContactIntent? contactIntent;
  const ContactsManagement({this.contactIntent, Key? key}) : super(key: key);
  @override
  ContactsManagementState createState() => ContactsManagementState();
}

class ContactsManagementState extends State<ContactsManagement> {
  late TextEditingController nameTextController;
  late TextEditingController addressTextController;
  @override
  void initState() {
    super.initState();
    final intent = widget.contactIntent ??
        AddContactIntent(contactAddress: '', contactName: '');
    this.nameTextController = TextEditingController(text: intent.contactName);
    this.addressTextController =
        TextEditingController(text: intent.contactAddress);
  }

  Widget buildCards(BuildContext context, List<Contact> contacts) {
    Iterable<Widget> contactWidgets = contacts.map(
      (contact) {
        return FutureBuilder<FileImage?>(
          future: ImageDb.getProfileImageForContact(contact.address),
          builder: (
            BuildContext context,
            AsyncSnapshot<FileImage?> snapshot,
          ) {
            return InkWell(
              child: Card(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    CircleAvatar(
                        backgroundImage: snapshot.data,
                        backgroundColor:
                            const Color.fromARGB(47, 127, 127, 127)),
                    Text(contact.name, style: const TextStyle(fontSize: 20)),
                  ],
                ),
              ),
              onTap: () {
                _navigateToContactPage(
                    context, contact.name, contact.address, snapshot.data);
              },
            );
          },
        );
      },
    );
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: contactWidgets.toList(),
    );
  }

  void _navigateToContactPage(
      BuildContext context, String name, String address, FileImage? image) {
    Navigator.of(context).push(MaterialPageRoute<void>(
        builder: (context) =>
            ContactPage(name: name, address: address, image: image)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Contact Management'),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Spacer(flex: 5),
              const Text('Contacts', style: TextStyle(fontSize: 24)),
              const Spacer(flex: 1),
              FutureBuilder<Iterable<Contact>>(
                future: ContactsData.getAllContacts(),
                builder: (
                  BuildContext context,
                  AsyncSnapshot<Iterable<Contact>> snapshot,
                ) {
                  if (snapshot.hasData) {
                    return buildCards(context, snapshot.data!.toList());
                  }
                  return const Text('Loading contacts...');
                },
              ),
              const Spacer(flex: 2),
              const Text('Add a new contact', style: TextStyle(fontSize: 24)),
              const SizedBox(height: 10),
              TextFormField(
                controller: nameTextController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Name',
                ),
              ),
              TextFormField(
                controller: addressTextController,
                decoration: const InputDecoration(
                  border: UnderlineInputBorder(),
                  labelText: 'Address',
                ),
              ),
              const SizedBox(height: 30),
              ElevatedButton(
                onPressed: () {
                  ContactsData.addContact(Contact(
                    nameTextController.text,
                    addressTextController.text,
                  ));
                  nameTextController.clear();
                  addressTextController.clear();
                  setState(() {});
                },
                child: const Text(
                  'Add contact...',
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
              const Spacer(flex: 5),
            ],
          ),
        ),
      ),
    );
  }
}

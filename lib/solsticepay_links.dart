import 'bridge_generated.dart' show SolanaPayRequestFFI;
import 'ffi.dart' show rust;

import 'settings/settings_data.dart' show SettingsData;

const solanaScheme = 'solana:';

Future<SolanaPayRequestFFI> generatePayRequestFromUri(
  String recipient,
  String queryParams,
) {
  final url = solanaScheme + recipient + '?' + queryParams;
  return rust.requestFromUrl(url: url);
}

Future<String> getPayUrl(
  String publicKey,
  double solAmount,
  String? message,
) async {
  final request = SolanaPayRequestFFI(
    recipient: publicKey,
    amount: solAmount,
    message: message,
    references: [],
  );
  final useSolanaScheme = await SettingsData.getUseSolanaScheme();
  String link = await rust.linkFromRequest(request: request);
  if (!useSolanaScheme) {
    link = link.replaceFirst(solanaScheme, 'https://link.solsticepay.com/pay/');
  }
  return link;
}

String getAddContactLink(
  String address,
  String name,
) {
  return Uri(
    scheme: 'https',
    host: 'link.solsticepay.com',
    path: 'add_contact',
    queryParameters: {
      'contact_name': name,
      'addr': address,
    },
  ).toString();
}

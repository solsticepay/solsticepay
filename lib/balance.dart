import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart' show SpinKitChasingDots;
import 'package:solsticepay/price_indicator.dart'
    show fetchSolanaPrice, getAmountInUsd;
import 'ffi.dart' show rust;
import 'private_key_management/private_key_manager.dart';

class Balance extends StatefulWidget {
  const Balance({Key? key}) : super(key: key);
  @override
  State<Balance> createState() => _BalanceState();
}

class _BalanceState extends State<Balance> {
  double? solPrice = null;
  Future<double?> getPrimaryBalance() async {
    final publicKey = await PrivateKeyManager.getPublicKey();
    if (publicKey == null) {
      throw Exception(
          'Wallet has not been initialized! Please register a private key in settings.');
    }
    return rust.getBalance(address: publicKey);
  }

  @override
  void initState() {
    fetchSolanaPrice('usd').then((p) => setState(() => this.solPrice = p));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        FutureBuilder<double?>(
          future: getPrimaryBalance(),
          builder: (
            BuildContext context,
            AsyncSnapshot<double?> snapshot,
          ) {
            final state = snapshot.connectionState;
            String text;
            double opacity = 0.6;
            double fontSize = 20.0;
            if (state == ConnectionState.done && !snapshot.hasData) {
              text = 'Register a private key in settings';
            } else if (state == ConnectionState.done && snapshot.hasData) {
              if (this.solPrice == null) {
                text = '${snapshot.data!.toStringAsFixed(3)} SOL';
              } else {
                text =
                    '\$${getAmountInUsd(snapshot.data!, this.solPrice!).toStringAsFixed(2)}';
              }

              opacity = 1.0;
              fontSize = 60.0;
            } else if (snapshot.hasError) {
              text = '${snapshot.error!}';
            } else {
              return SpinKitChasingDots(
                  color: Theme.of(context).colorScheme.primary);
            }

            return TextButton(
              onPressed: () => setState(() {}),
              child: Text(
                text,
                style: TextStyle(
                  fontSize: fontSize,
                  color: Theme.of(context)
                      .colorScheme
                      .onSurface
                      .withOpacity(opacity),
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}

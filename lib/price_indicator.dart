import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class PriceIndicator extends StatelessWidget {
  final double? solPrice;

  const PriceIndicator({this.solPrice, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          "Sol -> USD (\$${solPrice ?? 'ERR'})",
          style: const TextStyle(fontSize: 30),
        ),
        const Text(
          'price from coingecko.com',
          style: TextStyle(fontSize: 10),
        ),
      ],
    );
  }
}

class PriceConversionText extends StatelessWidget {
  final double? solPrice;
  final String value;

  const PriceConversionText({this.solPrice, this.value = '', Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      () {
        if (solPrice == null) {
          return 'Error fetching SOL price';
        }
        double? amount = getAmountInSol(this.value, solPrice!);
        if (amount == null) {
          return 'Invalid value';
        }
        return 'SOL price: $amount';
      }(),
    );
  }
}

Future<double> fetchSolanaPrice(String currency) {
  return http
      .get(Uri.parse('https://api.coingecko.com/api/v3/coins/solana'))
      .then((response) {
    if (response.statusCode == 200) {
      return jsonDecode(response.body)['market_data']['current_price'][currency]
          as double;
    } else {
      debugPrint(response.toString());
      throw Exception('Could not get SOL price');
    }
  });
}

Timer solanaPriceFetcher(
  String currency,
  Duration interval,
  void Function(double) callback,
) {
  fetchSolanaPrice(currency).then(callback);
  return Timer.periodic(
    interval,
    (_result) => fetchSolanaPrice(currency).then(callback),
  );
}

double? getAmountInSol(String usd, double solPrice) {
  usd = usd.isEmpty ? '0.0' : usd;
  double? parseAttempt = double.tryParse(usd);
  if (parseAttempt == null) {
    return null;
  }
  final usdAmount = parseAttempt;
  final solAmount = usdAmount / solPrice;
  return solAmount;
}

double getAmountInUsd(double solAmount, double solPrice) {
  return solAmount * solPrice;
}

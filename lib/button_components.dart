import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart' show SvgPicture;
import 'send_token.dart';
import 'request_token.dart' show RequestScreen;

const buttonTextSyle = TextStyle(fontSize: 20);

class ArrowSvg extends StatelessWidget {
  const ArrowSvg({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      'assets/button_arrow.svg',
      color: Theme.of(context).colorScheme.onPrimary,
      height: 15,
    );
  }
}

class SolsticeButtonProperties {
  static const textTopSpacer = Spacer(flex: 2);
  static const textMiddleSpacer = Spacer(flex: 2);
  static const textBottomSpacer = Spacer(flex: 2);
  static final buttonStyle = ElevatedButton.styleFrom(
    fixedSize: const Size(140.0, 70.0),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20.0),
    ),
    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
  );
}

class SendButton extends StatelessWidget {
  const SendButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: SolsticeButtonProperties.buttonStyle,
      child: Column(children: const <Widget>[
        SolsticeButtonProperties.textTopSpacer,
        ArrowSvg(),
        SolsticeButtonProperties.textMiddleSpacer,
        Text('Send', style: buttonTextSyle),
        SolsticeButtonProperties.textBottomSpacer,
      ]),
      onPressed: () {
        _navigateToSendTransactionScreen(context);
      },
    );
  }
}

class RequestButton extends StatelessWidget {
  const RequestButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: SolsticeButtonProperties.buttonStyle,
      child: Column(children: const <Widget>[
        SolsticeButtonProperties.textTopSpacer,
        RotatedBox(
          quarterTurns: 2,
          child: ArrowSvg(),
        ),
        SolsticeButtonProperties.textMiddleSpacer,
        Text('Request', style: buttonTextSyle),
        SolsticeButtonProperties.textBottomSpacer,
      ]),
      onPressed: () {
        _navigateToRequestTransactionScreen(context);
      },
    );
  }
}

void _navigateToSendTransactionScreen(BuildContext context) {
  Navigator.of(context)
      .push(MaterialPageRoute<void>(builder: (context) => const SendScreen()));
}

void _navigateToRequestTransactionScreen(BuildContext context) {
  Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (context) => const RequestScreen()));
}

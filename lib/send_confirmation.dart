import 'package:flutter/material.dart';
import 'ffi.dart' show rust;
import 'private_key_management/private_key_manager.dart';
import 'private_key_management/ask_user_to_decrypt_key.dart';

class SendConfirmationScreen extends StatelessWidget {
  const SendConfirmationScreen(
      this.messageJson, this.recipient, this.amount, this.transactionFee,
      {Key? key})
      : super(key: key);

  final String messageJson;
  final String recipient;
  final double amount;
  final double transactionFee;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sending SOL'),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
      ),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Spacer(),
              Text(
                'Recipient: $recipient\nSending: $amount\nTransaction fee: $transactionFee',
                style: const TextStyle(fontSize: 20.0),
              ),
              const Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context, false);
                    },
                    child: const Text(
                      'Cancel',
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      _doTransaction(context);
                    },
                    child: const Text(
                      'Confirm',
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _doTransaction(BuildContext context) async {
    final publicKey = await PrivateKeyManager.getPublicKey();
    if (publicKey == null) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(
          const SnackBar(
            content: Text(
                'Wallet has not been initialized! Please register a private key in settings.'),
          ),
        );
    } else {
      askUserToDecrypt(
        context,
        'Confirm transaction',
      ).then(
        (privateKey) {
          if (privateKey != null) {
            rust
                .finishTransaction(
                    privateKey: privateKey, messageJson: messageJson)
                .then((result) {
              ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(SnackBar(content: Text(result)));
              Navigator.pop(context, true);
            });
          }
        },
      );
    }
  }
}

import 'package:flutter/material.dart';
import 'button_components.dart' show RequestButton, SendButton;
import 'balance.dart' show Balance;
import 'settings/settings.dart' show SettingsPage;
import 'settings/settings_data.dart' show SettingsData;
import 'transaction_history/transaction_history.dart' show TransactionHistory;
import 'initial_setup/entry.dart' show InitialSetupScreen;
import 'initial_setup/prefs.dart' show didInitialSetup;
import 'image_db.dart' show ImageDb;
import 'profile/profile_hero_icon.dart' show ProfileHeroIcon;

class TransactionHistoryPanel extends StatelessWidget {
  const TransactionHistoryPanel({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Spacer(flex: 2),
        Row(
          children: [
            const Spacer(flex: 1),
            Text(
              'HISTORY',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).colorScheme.primary,
              ),
            ),
            const Spacer(flex: 1),
          ],
        ),
        const SizedBox(height: 5),
        const Expanded(
          flex: 100,
          child: TransactionHistory(),
        ),
      ],
    );
  }
}

class HomeTop extends StatelessWidget {
  const HomeTop({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Spacer(flex: 10),
        FutureBuilder<String>(
          future: SettingsData.getFirstName(),
          builder: (
            BuildContext context,
            AsyncSnapshot<String> snapshot,
          ) {
            if (snapshot.hasData && snapshot.data! != '') {
              return Text('Hello ${snapshot.data!}!',
                  style: const TextStyle(fontSize: 30.0));
            } else {
              return const Text('Hello!', style: TextStyle(fontSize: 30.0));
            }
          },
        ),
        const SizedBox(height: 1),
        const SizedBox(height: 100, child: Balance()),
        const Spacer(flex: 4),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: const [
          Spacer(flex: 8),
          SendButton(),
          Spacer(flex: 5),
          RequestButton(),
          Spacer(flex: 8),
        ]),
        const Spacer(flex: 8),
      ],
    );
  }
}

class HomeBottom extends StatelessWidget {
  const HomeBottom({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Container background = Container(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      color: Theme.of(context).colorScheme.secondaryContainer,
    );
    return Stack(
      children: [
        background,
        // TODO replace with right and left scrolling for other panels.
        const TransactionHistoryPanel(),
      ],
    );
  }
}

class MyHome extends StatelessWidget {
  const MyHome({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: didInitialSetup(),
      builder: (
        BuildContext context,
        AsyncSnapshot<bool> snapshot,
      ) {
        if (snapshot.hasData) {
          final finishedInitialSetup = snapshot.data!;
          if (finishedInitialSetup) {
            return initializedHomeScreen(context);
          } else {
            return const InitialSetupScreen();
          }
        } else {
          // Empty component while waiting for data
          return const SizedBox.shrink();
        }
      },
    );
  }

  Widget initializedHomeScreen(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text(
                'Solstice Pay',
                style: TextStyle(
                  fontSize: 24.0,
                  color: Theme.of(context).colorScheme.primary,
                ),
              ),
              backgroundColor: Colors.transparent,
              shadowColor: Colors.transparent,
              actions: <Widget>[
                TextButton(
                  child: const Icon(Icons.settings),
                  onPressed: () {
                    navigateToSettingsPage(context);
                  },
                ),
              ],
            ),
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Expanded(
                    flex: 40,
                    child: HomeTop(),
                  ),
                  Expanded(
                    flex: 60,
                    child: HomeBottom(),
                  ),
                ],
              ),
            ),
          ),
        ),
        SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              children: [
                FutureBuilder<FileImage?>(
                  future: ImageDb.getMainProfileImage(),
                  builder: (
                    BuildContext context,
                    AsyncSnapshot<FileImage?> snapshot,
                  ) {
                    return ProfileHeroIcon(snapshot.data);
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  void navigateToSettingsPage(BuildContext context) {
    Navigator.of(context).push(
        MaterialPageRoute<void>(builder: (context) => const SettingsPage()));
  }
}

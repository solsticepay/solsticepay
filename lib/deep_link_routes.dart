import 'package:flutter/material.dart';
import 'solsticepay_links.dart' show generatePayRequestFromUri;
import 'dart:async';

import 'package:uni_links/uni_links.dart';
import 'package:flutter/services.dart' show PlatformException;
import 'contacts_management.dart';
import 'send_token.dart';

const kIsWeb = false;
bool isValidParams(Set<String> validKeys, Iterable<String> paramsKeys) {
  return validKeys.length == paramsKeys.length &&
      paramsKeys.toSet().containsAll(validKeys);
}

class DeepLinkRouteHandler {
  DeepLinkRouteHandler(this.navigatorKey) {
    _handleIncomingLinks();
    _handleInitialUri();
  }
  GlobalKey<NavigatorState> navigatorKey;
  bool _initialUriIsHandled = false;

  StreamSubscription<Uri?>? _sub;

  void dispose() {
    _sub?.cancel();
  }

  void pushWidgetOnNavigator(Widget widget) {
    this.navigatorKey.currentState!.push(
          MaterialPageRoute<void>(
            builder: (context) => widget,
          ),
        );
  }

  void handlePayRequest(Uri uri) {
    generatePayRequestFromUri(uri.path.replaceFirst('/', ''), uri.query)
        .then((request) {
      pushWidgetOnNavigator(SendScreen(request: request));
    });
  }

  void handleAddContact(Map<String, String> params) {
    final validKeys = {'contact_name', 'addr'};
    if (isValidParams(validKeys, params.keys)) {
      pushWidgetOnNavigator(ContactsManagement(
        contactIntent: AddContactIntent(
          contactName: (params['contact_name'] ?? 'Unknown Name')
              .replaceAll(RegExp('_'), ' '),
          contactAddress: params['addr'] ?? 'Unkonwn Address',
        ),
      ));
    }
  }

  Future<void> handleURIRequest(Uri uri) async {
    String path;
    if (uri.scheme == 'solstice') {
      path = uri.host;
    } else if (uri.host == 'link.solsticepay.com') {
      path = uri.path.startsWith('/') ? uri.path.substring(1) : uri.path;
    } else {
      throw Exception('unknown origin');
    }
    final params = uri.queryParameters;
    if (path == 'pay') {
      handlePayRequest(uri);
    } else if (path == 'add_contact') {
      handleAddContact(params);
    } else {
      debugPrint('unknown path $path');
    }
  }

  Future<void> _handleInitialUri() async {
    if (!_initialUriIsHandled) {
      _initialUriIsHandled = true;
      try {
        final uri = await getInitialUri();
        if (uri == null) {
          debugPrint('no initial uri');
        } else {
          handleURIRequest(uri);
        }
      } on PlatformException {
        // Platform messages may fail but we ignore the exception
        debugPrint('falied to get initial uri');
      } on FormatException catch (err) {
        debugPrint('malformed initial uri $err');
      }
    }
  }

  /// Handle incoming links - the ones that the app will recieve from the OS
  /// while already started.
  void _handleIncomingLinks() {
    if (!kIsWeb) {
      // solstice://request_from_usd?usd_amt=16&addr=0x69&token=SOL

      // It will handle app links while the app is already started - be it in
      // the foreground or in the background.
      _sub = uriLinkStream.listen(
        (Uri? uri) {
          if (uri == null) {
            return;
          }
          handleURIRequest(uri);
        },
        onError: (Object err) {
          debugPrint('got err: $err');
        },
      );
    }
  }
}

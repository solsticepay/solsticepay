import 'package:flutter/material.dart';
import '../bridge_generated.dart' show Transaction;
import 'package:intl/intl.dart' show DateFormat;

class DateField extends StatelessWidget {
  final Transaction transaction;

  const DateField({required this.transaction, Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      DateFormat('MM/dd/yy').format(DateTime.fromMillisecondsSinceEpoch(
          this.transaction.unixEpochTimeStamp! * 1000)),
      style: TextStyle(
        fontSize: 14,
        color: Theme.of(context).colorScheme.onSecondary,
      ),
    );
  }
}

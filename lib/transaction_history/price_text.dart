import 'package:flutter/material.dart';
import '../bridge_generated.dart' show Transaction;
import '../price_indicator.dart' show getAmountInUsd;

class PriceText extends StatelessWidget {
  final Transaction transaction;
  final double? solPrice;
  final bool isReceiver;
  final bool darkMode;

  const PriceText(
      {required this.transaction,
      required this.solPrice,
      required this.isReceiver,
      required this.darkMode,
      Key? key})
      : super(key: key);

  Color priceColor() {
    if (this.darkMode) {
      if (this.isReceiver) {
        return const Color.fromARGB(255, 0, 220, 118);
      } else {
        return const Color.fromARGB(255, 255, 30, 30);
      }
    } else {
      if (this.isReceiver) {
        return const Color.fromARGB(255, 29, 162, 76);
      } else {
        return const Color.fromARGB(255, 213, 18, 24);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double amount = transaction.transactionAmount;
    bool hasSolPrice = this.solPrice != null;
    if (hasSolPrice) {
      amount = getAmountInUsd(transaction.transactionAmount, this.solPrice!);
    }
    TextStyle style = TextStyle(
      fontSize: 16,
      color: priceColor(),
      fontWeight: FontWeight.w500,
    );
    return SizedBox(
      width: 100,
      child: Row(
        children: [
          Text((hasSolPrice ? '\$' : ''), style: style),
          Text(amount.toStringAsFixed(hasSolPrice ? 2 : 5), style: style),
          const Spacer(flex: 3),
          Text(hasSolPrice ? '' : 'SOL', style: style),
          const Spacer(flex: 80),
        ],
      ),
    );
  }
}

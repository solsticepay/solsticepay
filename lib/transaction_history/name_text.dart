import 'package:flutter/material.dart';
import '../bridge_generated.dart' show Transaction;
import '../contacts_data.dart' show ContactsData;

class NameText extends StatelessWidget {
  final Transaction transaction;
  final bool isReceiver;
  final String otherWalletPubkey;

  const NameText({
    required this.transaction,
    required this.isReceiver,
    required this.otherWalletPubkey,
    Key? key,
  }) : super(key: key);

  Widget buildNameText(BuildContext context, AsyncSnapshot<String?> snapshot) {
    int len = 5;
    String nameText = this.otherWalletPubkey.substring(0, len) +
        '..' +
        this.otherWalletPubkey.substring(this.otherWalletPubkey.length - len);
    if (snapshot.hasData) {
      nameText = snapshot.data!;
    }
    return Row(
      children: [
        Text(
          this.isReceiver ? 'from' : 'to',
          style: TextStyle(
            fontSize: 14,
            color: Theme.of(context).colorScheme.onSecondary,
          ),
        ),
        const SizedBox(width: 2),
        Expanded(
          flex: 30,
          child: Text(
            nameText,
            maxLines: 1,
            softWrap: false,
            overflow: TextOverflow.fade,
            style: TextStyle(
              color: Theme.of(context).colorScheme.onPrimary,
              fontWeight: FontWeight.w600,
              fontSize: 14,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String?>(
      future: ContactsData.getName(this.otherWalletPubkey),
      builder: this.buildNameText,
    );
  }
}

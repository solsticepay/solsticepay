import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../bridge_generated.dart' show Transaction;
import '../ffi.dart' show rust;
import '../price_indicator.dart' show fetchSolanaPrice;
import '../private_key_management/private_key_manager.dart'
    show PrivateKeyManager;
import 'gradient_overlay.dart';
import '../settings/settings_data.dart' show SettingsData;
import 'transaction_card.dart' show TransactionCard;
import 'package:flutter_spinkit/flutter_spinkit.dart';

const historyLength = 15;

Future<List<Transaction>?> getTransactions() async {
  final address = await PrivateKeyManager.getPublicKey();
  if (address == null) {
    throw Exception('no address found!');
  }
  return await rust.processTransactionHistory(
    address: address,
    limit: historyLength,
  );
}

// Used to build each Transaction card.
class TransactionCardFactory {
  final List<Transaction>? transactions;
  final String pubkey;
  final double? solPrice;
  final bool darkMode;

  TransactionCardFactory({
    required this.transactions,
    required this.pubkey,
    required this.solPrice,
    required this.darkMode,
  });
  Widget builderDelegate(BuildContext context, int index) {
    if (this.transactions != null && index < this.transactions!.length) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(30, 5, 30, 5),
        child: TransactionCard(
          transaction: this.transactions![index],
          pubkey: this.pubkey,
          solPrice: this.solPrice,
          darkMode: this.darkMode,
        ),
      );
    } else {
      throw Exception('index out of bounds');
    }
  }
}

class TransactionHistory extends StatefulWidget {
  const TransactionHistory({Key? key}) : super(key: key);
  @override
  TransactionHistoryState createState() => TransactionHistoryState();
}

class TransactionHistoryState extends State {
  List<Transaction>? transactions = null;
  String? pubkey = null;
  bool? isDarkMode = null;
  double? solPrice = null;
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  bool isReady() {
    return this.pubkey != null &&
        this.transactions != null &&
        this.isDarkMode != null;
  }

  @override
  void initState() {
    getTransactions().then(
      (transactions) {
        setState(() => this.transactions = transactions);
        SettingsData.getUseDarkMode()
            .then((darkMode) => setState(() => this.isDarkMode = darkMode));
        fetchSolanaPrice('usd')
            .then((price) => setState(() => this.solPrice = price));
        PrivateKeyManager.getPublicKey()
            .then((pubkey) => setState(() => this.pubkey = pubkey));
      },
    );

    super.initState();
  }

  void _onRefresh() async {
    final _transactions = await getTransactions();
    _refreshController.refreshCompleted();
    if (_transactions == null) {
      _refreshController.refreshFailed();
    } else {
      _refreshController.refreshCompleted();
      setState(() => this.transactions = _transactions);
    }
  }

  Widget transactionPanelWithGradients(BuildContext context) {
    return GradientOverlay(
      child: Scaffold(
        backgroundColor: Theme.of(context).colorScheme.secondaryContainer,
        body: SmartRefresher(
          enablePullDown: true,
          header: WaterDropHeader(
            waterDropColor: Theme.of(context).colorScheme.primary,
            failed: const Text('Error fetching history - try again'),
            complete: const Text(
              '🚀',
              style: TextStyle(fontSize: 20),
            ),
            completeDuration: const Duration(seconds: 1),
          ),
          controller: _refreshController,
          onRefresh: this._onRefresh,
          child: ListView.builder(
            padding: const EdgeInsets.fromLTRB(0, 15, 0, 75),
            itemBuilder: TransactionCardFactory(
              transactions: this.transactions,
              pubkey: this.pubkey!,
              solPrice: this.solPrice,
              darkMode: this.isDarkMode!,
            ).builderDelegate,
            itemExtent: 100.0,
            itemCount: this.transactions!.length,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget body = this.isReady()
        ? this.transactionPanelWithGradients(context)
        : Center(
            child: SpinKitChasingDots(
                color: Theme.of(context).colorScheme.primary),
          );

    return body;
  }
}

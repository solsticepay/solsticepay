import 'package:flutter/material.dart';

class _Gradient extends StatelessWidget {
  final double startStop;
  final double endStop;
  final FractionalOffset end;
  final FractionalOffset begin;
  const _Gradient(
      {required this.startStop,
      required this.endStop,
      required this.end,
      required this.begin,
      Key? key})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: Container(
        height: 100,
        decoration: BoxDecoration(
          color: Colors.white,
          gradient: LinearGradient(
            begin: begin,
            end: end,
            colors: [
              Theme.of(context).colorScheme.secondaryContainer.withOpacity(0.0),
              Theme.of(context).colorScheme.secondaryContainer,
            ],
            stops: [startStop, endStop],
          ),
        ),
      ),
    );
  }
}

class GradientOverlay extends StatelessWidget {
  final Widget child;
  const GradientOverlay({required this.child, Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.topCenter,
      children: [
        Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            child,
            const _Gradient(
              startStop: 0.0,
              endStop: 0.85,
              begin: FractionalOffset.topCenter,
              end: FractionalOffset.bottomCenter,
            ),
          ],
        ),
        const _Gradient(
          startStop: 0.9,
          endStop: 0.99,
          begin: FractionalOffset.bottomCenter,
          end: FractionalOffset.topCenter,
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import '../bridge_generated.dart' show Transaction;
import 'date_field.dart' show DateField;
import 'name_text.dart' show NameText;
import 'price_text.dart' show PriceText;
import '../image_db.dart' show ImageDb;

class TransactionCard extends StatelessWidget {
  final Transaction transaction;
  final String pubkey;
  final double? solPrice;
  final bool darkMode;

  const TransactionCard(
      {required this.transaction,
      required this.pubkey,
      required this.solPrice,
      required this.darkMode,
      Key? key})
      : super(key: key);

  Widget _profilePicture(String pubkey) {
    return Container(
        height: 50.0,
        width: 50.0,
        decoration: BoxDecoration(
          color: const Color.fromARGB(0, 255, 255, 255),
          borderRadius: const BorderRadius.all(Radius.circular(50.0)),
          border: Border.all(color: const Color.fromARGB(0, 40, 38, 63)),
        ),
        child: FutureBuilder<FileImage?>(
            future: ImageDb.getProfileImageForContact(pubkey),
            builder: (
              BuildContext context,
              AsyncSnapshot<FileImage?> snapshot,
            ) {
              if (snapshot.hasData) {
                return CircleAvatar(
                  backgroundImage: snapshot.data,
                  backgroundColor: const Color.fromARGB(47, 127, 127, 127),
                );
              } else {
                return const CircleAvatar(
                  backgroundColor: Color.fromARGB(47, 127, 127, 127),
                );
              }
            }));
  }

  Widget innerCard() {
    const sidePadding = 1;
    bool receiver = transaction.receiverPubkey == this.pubkey;
    Widget price = PriceText(
      solPrice: this.solPrice,
      transaction: transaction,
      isReceiver: receiver,
      darkMode: this.darkMode,
    );
    Widget date = DateField(transaction: this.transaction);
    final otherWalletPubkey =
        receiver ? transaction.senderPubkey : transaction.receiverPubkey;
    Widget name = NameText(
      isReceiver: receiver,
      transaction: this.transaction,
      otherWalletPubkey: otherWalletPubkey,
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const Spacer(flex: sidePadding),
        this._profilePicture(otherWalletPubkey),
        const Spacer(flex: 1),
        Expanded(
          flex: 20,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Spacer(flex: 3),
              Expanded(
                flex: 5,
                child: Row(children: [price, const Spacer(), date]),
              ),
              const Spacer(flex: 1),
              name,
              const Spacer(flex: 3),
            ],
          ),
        ),
        const Spacer(flex: sidePadding),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      decoration: BoxDecoration(
        color: Theme.of(context).cardTheme.color!,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        border: Border.all(
            width: 3.0, color: Theme.of(context).cardTheme.shadowColor!),
      ),
      height: 75,
      child: this.innerCard(),
    );
  }
}

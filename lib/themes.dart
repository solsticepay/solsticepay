import 'package:flutter/material.dart';
import 'settings/settings_data.dart' show SettingsData;

/// Used to update themes on the fly
class ThemeHandler with ChangeNotifier {
  ThemeMode themeMode = ThemeMode.system;
  ThemeHandler() {
    SettingsData.getUseDarkMode().then((value) => setDarkMode(value));
  }

  ThemeMode currentTheme() {
    return this.themeMode;
  }

  void setDarkMode(bool darkMode) {
    this.switchTheme(darkMode ? ThemeMode.dark : ThemeMode.light);
  }

  void switchTheme(ThemeMode themeMode) {
    this.themeMode = themeMode;
    notifyListeners();
  }
}

ThemeHandler themeHandler = ThemeHandler();

/// Assembles a theme for the app given only the values that differ between dark and light modes.
ThemeData _makeTheme({
  required bool dark,
  required Color background,
  required Color accentBackground,
  required Color primary,
  required Color secondary,
  required Color text,
  required Color card,
  required Color cardShadow,
  required Color onSecondary,
}) {
  final ThemeData from = dark ? ThemeData.dark() : ThemeData.light();
  final brightness = dark ? Brightness.dark : Brightness.light;
  return from.copyWith(
    scaffoldBackgroundColor: background,
    colorScheme: ColorScheme(
      brightness: brightness,
      background: background,
      onBackground: text,
      primary: primary,
      onPrimary: text,
      // TODO: the rest of these arguments should have unique colors
      secondary: secondary,
      onSecondary: onSecondary,
      error: primary,
      onError: text,
      surface: background,
      onSurface: text,
      secondaryContainer: accentBackground,
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        textStyle: const TextStyle(
          fontFamily: 'Ubuntu',
          fontSize: 24.0,
          fontWeight: FontWeight.w600,
        ),
        padding: const EdgeInsets.symmetric(
          vertical: 16,
          horizontal: 20,
        ),
      ),
    ),
    drawerTheme: DrawerThemeData(
      backgroundColor: background,
    ),
    cardTheme: CardTheme(
      color: card,
      shadowColor: cardShadow,
    ),
    textTheme: from.textTheme.apply(
      fontFamily: 'Ubuntu',
      bodyColor: text,
      displayColor: text,
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(color: primary.withOpacity(0.9)),
      hintStyle: TextStyle(color: text.withOpacity(0.4)),
    ),
    canvasColor: background,
  );
}

final ThemeData lightTheme = _makeTheme(
  dark: false,
  background: const Color(0xffffffff),
  accentBackground: const Color.fromARGB(255, 248, 246, 251),
  primary: const Color.fromARGB(255, 212, 202, 232),
  secondary: const Color.fromARGB(255, 155, 145, 188),
  text: const Color.fromARGB(255, 49, 54, 71),
  card: const Color.fromARGB(255, 250, 249, 252),
  cardShadow: const Color.fromARGB(255, 255, 255, 255),
  onSecondary: const Color.fromARGB(255, 128, 128, 128),
);

final ThemeData darkTheme = _makeTheme(
  dark: true,
  background: const Color.fromARGB(255, 7, 15, 43),
  accentBackground: const Color.fromARGB(255, 20, 27, 60),
  primary: const Color.fromARGB(255, 72, 74, 129),
  secondary: const Color.fromARGB(255, 72, 74, 129),
  text: const Color.fromARGB(255, 201, 220, 240),
  card: const Color.fromARGB(255, 42, 47, 79),
  cardShadow: const Color.fromARGB(255, 79, 80, 137),
  onSecondary: const Color.fromARGB(255, 137, 151, 176),
);

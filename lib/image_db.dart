import 'package:flutter/material.dart' show FileImage;
import 'dart:io';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart'
    show getApplicationSupportDirectory;
import 'ffi.dart' show rust;

const mainProfileImageFile = 'main_profile_image.webp';

const contactProfileImageDirectory = 'contact_profile_images';

class ImageDb {
  static Future<FileImage?> getMainProfileImage() async {
    final directory = await getApplicationSupportDirectory();
    final mainProfileImagePath = p.join(directory.path, mainProfileImageFile);
    return FileImage(File(mainProfileImagePath));
  }

  static Future<void> saveMainProfileImage(String chosenFilePath) async {
    final directory = await getApplicationSupportDirectory();
    final mainProfileImagePath = p.join(directory.path, mainProfileImageFile);

    await rust.normalizeImage(
        oldPath: chosenFilePath, newPath: mainProfileImagePath);

    FileImage(File(mainProfileImagePath)).evict();
  }

  static Future<FileImage?> getProfileImageForContact(String address) async {
    final directory = await getApplicationSupportDirectory();
    final profileImagePath =
        p.join(directory.path, contactProfileImageDirectory, '$address.webp');
    final imageFile = File(profileImagePath);
    if (!await imageFile.exists()) {
      return null;
    }
    return FileImage(imageFile);
  }

  static Future<void> saveProfileImageForContact(
      String chosenFilePath, String address) async {
    final directory = p.join((await getApplicationSupportDirectory()).path,
        contactProfileImageDirectory);
    await Directory(directory).create(recursive: true);
    final profileImagePath = p.join(directory, '$address.webp');

    await rust.normalizeImage(
        oldPath: chosenFilePath, newPath: profileImagePath);

    FileImage(File(profileImagePath)).evict();
  }
}

import 'package:flutter/material.dart';
import 'home_page.dart';
import 'deep_link_routes.dart';
import 'themes.dart' show lightTheme, darkTheme, themeHandler;

import 'dart:io' as io;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  DeepLinkRouteHandler? deepLinkRouter = null;

  @override
  void dispose() {
    if (deepLinkRouter != null) {
      deepLinkRouter!.dispose();
    }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    themeHandler.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    if (io.Platform.isAndroid || io.Platform.isIOS) {
      deepLinkRouter = DeepLinkRouteHandler(navigatorKey);
    } else {
      debugPrint('Platform does not support deeplinks or uni');
    }

    // Why we use a Navigator Key https://github.com/brianegan/flutter_redux/issues/5#issuecomment-361215074
    return MaterialApp(
      theme: lightTheme,
      darkTheme: darkTheme,
      themeMode: themeHandler.themeMode,
      title: 'Solstice Pay',
      home: const MyHome(),
      navigatorKey: navigatorKey,
    );
  }
}

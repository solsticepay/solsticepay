import 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;
import 'dart:convert' show jsonEncode, jsonDecode, json;
import 'package:collection/collection.dart'; // firstWhereOrNull

class Contact {
  final String name;
  final String address;

  Contact(this.name, this.address);

  Contact.fromJson(Map<String, dynamic> json)
      : name = json[ContactsData.nameKey] as String,
        address = json[ContactsData.addressKey] as String;

  Map<String, dynamic> toJson() {
    return {
      ContactsData.nameKey: name,
      ContactsData.addressKey: address,
    };
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other is Contact &&
            other.name == this.name &&
            other.address == this.address);
  }

  @override
  int get hashCode => Object.hash(name, address);
}

/// Manages contacts stored in the shared_preferences database
class ContactsData {
  static const sharedPreferencesKey = 'contact_data';
  static const nameKey = 'name';
  static const addressKey = 'address';

  static Future<String?> getName(String address) async {
    Iterable<Contact> contacts = await ContactsData.getAllContacts();
    for (Contact contact in contacts) {
      if (contact.address == address) {
        return contact.name;
      }
    }
    return null;
  }

  static Future<Iterable<Contact>> getAllContacts() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? shared = prefs.getString(ContactsData.sharedPreferencesKey);
    if (shared == null) {
      return [];
    } else {
      return (json.decode(shared) as List<dynamic>)
          .map((contact) => Contact.fromJson(contact as Map<String, dynamic>));
    }
  }

  static Future<Contact?> getAddress(String name) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? sharedJson = prefs.getString(ContactsData.sharedPreferencesKey);
    final contactList = jsonDecode(sharedJson!) as List<Contact>;
    final matchingContact =
        contactList.firstWhereOrNull((curr) => curr.name == name);
    return matchingContact;
  }

  // TODO use a unique identifier for each contact.
  // This may delete multiple contacts if they all have the same name.
  static Future<void> removeContact(String name) async {
    final contactList =
        (await getAllContacts()).where((c) => c.name != name).toList();

    (await SharedPreferences.getInstance()).setString(
      ContactsData.sharedPreferencesKey,
      jsonEncode(contactList),
    );
  }

  static void addContact(Contact contact) async {
    final contactList = (await getAllContacts()).toList();
    contactList.add(contact);

    (await SharedPreferences.getInstance()).setString(
      ContactsData.sharedPreferencesKey,
      jsonEncode(contactList),
    );
  }
}

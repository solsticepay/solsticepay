#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct wire_uint_8_list {
  uint8_t *ptr;
  int32_t len;
} wire_uint_8_list;

typedef struct wire_StringList {
  struct wire_uint_8_list **ptr;
  int32_t len;
} wire_StringList;

typedef struct wire_SolanaPayRequestFFI {
  struct wire_uint_8_list *recipient;
  double *amount;
  struct wire_uint_8_list *spl_token;
  struct wire_StringList *references;
  struct wire_uint_8_list *label;
  struct wire_uint_8_list *message;
  struct wire_uint_8_list *memo;
} wire_SolanaPayRequestFFI;

typedef struct WireSyncReturnStruct {
  uint8_t *ptr;
  int32_t len;
  bool success;
} WireSyncReturnStruct;

typedef int64_t DartPort;

typedef bool (*DartPostCObjectFnType)(DartPort port_id, void *message);

void wire_normalize_image(int64_t port_,
                          struct wire_uint_8_list *old_path,
                          struct wire_uint_8_list *new_path);

void wire_generate_new_private_key(int64_t port_);

void wire_get_random_mnemonic_phrase(int64_t port_);

void wire_mnemonic_to_private_key(int64_t port_,
                                  struct wire_uint_8_list *phrase,
                                  struct wire_uint_8_list *passphrase);

void wire_get_balance(int64_t port_, struct wire_uint_8_list *address);

void wire_create_transaction(int64_t port_,
                             struct wire_uint_8_list *sender_base58_pubkey,
                             double amount,
                             struct wire_uint_8_list *recipient_base58_pubkey);

void wire_finish_transaction(int64_t port_,
                             struct wire_uint_8_list *private_key,
                             struct wire_uint_8_list *message_json);

void wire_private_key_to_pubkey(int64_t port_, struct wire_uint_8_list *private_key);

void wire_process_transaction_history(int64_t port_,
                                      struct wire_uint_8_list *address,
                                      uint16_t limit);

void wire_link_from_request(int64_t port_, struct wire_SolanaPayRequestFFI *request);

void wire_request_from_url(int64_t port_, struct wire_uint_8_list *url);

struct wire_StringList *new_StringList(int32_t len);

double *new_box_autoadd_f64(double value);

struct wire_SolanaPayRequestFFI *new_box_autoadd_solana_pay_request_ffi(void);

struct wire_uint_8_list *new_uint_8_list(int32_t len);

void free_WireSyncReturnStruct(struct WireSyncReturnStruct val);

void store_dart_post_cobject(DartPostCObjectFnType ptr);

static int64_t dummy_method_to_enforce_bundling(void) {
    int64_t dummy_var = 0;
    dummy_var ^= ((int64_t) (void*) wire_normalize_image);
    dummy_var ^= ((int64_t) (void*) wire_generate_new_private_key);
    dummy_var ^= ((int64_t) (void*) wire_get_random_mnemonic_phrase);
    dummy_var ^= ((int64_t) (void*) wire_mnemonic_to_private_key);
    dummy_var ^= ((int64_t) (void*) wire_get_balance);
    dummy_var ^= ((int64_t) (void*) wire_create_transaction);
    dummy_var ^= ((int64_t) (void*) wire_finish_transaction);
    dummy_var ^= ((int64_t) (void*) wire_private_key_to_pubkey);
    dummy_var ^= ((int64_t) (void*) wire_process_transaction_history);
    dummy_var ^= ((int64_t) (void*) wire_link_from_request);
    dummy_var ^= ((int64_t) (void*) wire_request_from_url);
    dummy_var ^= ((int64_t) (void*) new_StringList);
    dummy_var ^= ((int64_t) (void*) new_box_autoadd_f64);
    dummy_var ^= ((int64_t) (void*) new_box_autoadd_solana_pay_request_ffi);
    dummy_var ^= ((int64_t) (void*) new_uint_8_list);
    dummy_var ^= ((int64_t) (void*) free_WireSyncReturnStruct);
    dummy_var ^= ((int64_t) (void*) store_dart_post_cobject);
    return dummy_var;
}
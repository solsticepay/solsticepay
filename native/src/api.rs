use anyhow::Result;
use std::str::FromStr;

use bip39::{Language, Mnemonic, MnemonicType};
use solana_pay::SolanaPayRequest;
use solsticepay_core as core;
use url::Url;

const RPC_ENDPOINT: &str = "https://api.mainnet-beta.solana.com";

/// Copies the image at `old_path` into `new_path`, but encoded as a square webp with a maximum of
/// 1000px dimensions.
pub fn normalize_image(old_path: String, new_path: String) -> Result<()> {
    use image::EncodableLayout;
    use std::io::Write;

    let img = image::open(old_path).map_err(|_| anyhow::anyhow!("unable to decode image"))?;

    let width = img.width();
    let height = img.height();

    let new_dim = width.min(height).min(1000);
    let img = img.resize_to_fill(new_dim, new_dim, image::imageops::FilterType::CatmullRom);

    let webp_encoder =
        webp::Encoder::from_image(&img).map_err(|_| anyhow::anyhow!("unable to encode webp"))?;
    let memory = webp_encoder.encode(85.);

    let mut f = std::fs::File::create(new_path)?;
    f.write_all(memory.as_bytes())?;
    f.sync_all()?;
    Ok(())
}

pub fn generate_new_private_key() -> String {
    core::Keypair::new().to_base58_string()
}

pub fn get_random_mnemonic_phrase() -> String {
    Mnemonic::new(MnemonicType::Words12, Language::English)
        .phrase()
        .to_string()
}

// Generates a new Keypair with a mnemonic phrase + passphrase.
pub fn mnemonic_to_private_key(phrase: String, passphrase: String) -> Result<String> {
    Ok(
        core::keypair_from_seed_phrase_and_passphrase(&phrase, &passphrase)
            .map_err(|_| anyhow::anyhow!("unable to get private key from seed phrase"))?
            .to_base58_string(),
    )
}

pub fn get_balance(address: String) -> Result<f64> {
    core::get_balance(RPC_ENDPOINT, &address).map_err(|e| anyhow::anyhow!(e))
}

pub struct PreparedTransaction {
    pub message_json: String,
    pub fee: f64,
}

impl From<core::PreparedTransaction> for PreparedTransaction {
    fn from(v: core::PreparedTransaction) -> Self {
        PreparedTransaction {
            message_json: serde_json::to_string(&v.message).unwrap(),
            fee: v.fee,
        }
    }
}

pub fn create_transaction(
    sender_base58_pubkey: String,
    amount: f64,
    recipient_base58_pubkey: String,
) -> Result<PreparedTransaction> {
    let sender_pubkey =
        core::Pubkey::from_str(&sender_base58_pubkey).expect("could not parse sender pubkey");
    let recipient_pubkey =
        core::Pubkey::from_str(&recipient_base58_pubkey).expect("could not parse recipient pubkey");
    core::create_transaction(RPC_ENDPOINT, &sender_pubkey, amount, &recipient_pubkey)
        .map(|p| p.into())
        .map_err(|e| anyhow::anyhow!(e))
}

pub fn finish_transaction(private_key: String, message_json: String) -> Result<String> {
    let private_key = core::Keypair::from_base58_string(&private_key);
    let message = serde_json::from_str(&message_json).unwrap();
    core::finish_transaction(RPC_ENDPOINT, &private_key, message).map_err(|e| anyhow::anyhow!(e))
}

pub fn private_key_to_pubkey(private_key: String) -> String {
    core::private_key_to_pubkey(&private_key)
}

pub struct Transaction {
    pub sender_pubkey: String,
    pub receiver_pubkey: String,
    pub transaction_amount: f64,
    pub transaction_fee: f64,
    pub unix_epoch_time_stamp: Option<i64>,
    pub sender_pre_amount: f64,
    pub sender_post_amount: f64,
    pub receiver_pre_amount: f64,
    pub receiver_post_amount: f64,
}

pub fn process_transaction_history(address: String, limit: u16) -> Result<Vec<Transaction>> {
    let address = core::Pubkey::from_str(&address).expect("Invalid pubkey for spl_token");
    Ok(core::process_transaction_history(
        &core::RpcClient::new(RPC_ENDPOINT.to_string()),
        &address,
        None,
        None,
        limit as usize,
    )
    .map_err(|op| anyhow::anyhow!(op.to_string()))?
    .into_iter()
    .map(|transaction| {
        let metadata = transaction
            .transaction
            .meta
            .expect("Unable to get metadata for transaction");
        let timestamp = transaction.block_time;
        let transaction = transaction.transaction.transaction.decode().unwrap();

        assert!(transaction.message.account_keys.len() >= 2);
        let sender_pre_amount = &metadata.pre_balances[0];
        let receiver_pre_amount = &metadata.pre_balances[1];
        let sender_post_amount = &metadata.post_balances[0];
        let receiver_post_amount = &metadata.post_balances[1];
        let txn_amount = receiver_post_amount - receiver_pre_amount;
        let sender_address = transaction.message.account_keys[0].to_string();
        let receiver_address = transaction.message.account_keys[1].to_string();

        Transaction {
            sender_pubkey: sender_address,
            receiver_pubkey: receiver_address,
            transaction_amount: core::lamports_to_sol(txn_amount),
            transaction_fee: core::lamports_to_sol(metadata.fee),
            unix_epoch_time_stamp: timestamp,
            sender_pre_amount: core::lamports_to_sol(*sender_pre_amount),
            sender_post_amount: core::lamports_to_sol(*sender_post_amount),
            receiver_pre_amount: core::lamports_to_sol(*receiver_pre_amount),
            receiver_post_amount: core::lamports_to_sol(*receiver_post_amount),
        }
    })
    .collect::<Vec<Transaction>>())
}
pub struct SolanaPayRequestFFI {
    pub recipient: String,
    pub amount: Option<f64>,
    pub spl_token: Option<String>,
    pub references: Vec<String>,
    pub label: Option<String>,
    pub message: Option<String>,
    pub memo: Option<String>,
}

impl From<SolanaPayRequestFFI> for SolanaPayRequest {
    fn from(request: SolanaPayRequestFFI) -> SolanaPayRequest {
        SolanaPayRequest {
            recipient: core::Pubkey::from_str(&request.recipient)
                .expect("Invalid pubkey for recipient"),
            amount: request.amount,
            spl_token: request
                .spl_token
                .map(|o| core::Pubkey::from_str(&o).expect("Invalid pubkey for spl_token")),
            references: request
                .references
                .iter()
                .map(|r| core::Pubkey::from_str(r).expect("Invalid pubkey for reference"))
                .collect(),
            label: request.label,
            message: request.message,
            memo: request.memo,
            extensions: vec![],
        }
    }
}

impl From<SolanaPayRequest> for SolanaPayRequestFFI {
    fn from(request: SolanaPayRequest) -> SolanaPayRequestFFI {
        SolanaPayRequestFFI {
            recipient: request.recipient.to_string(),
            amount: request.amount,
            spl_token: request.spl_token.map(|o| o.to_string()),
            references: request.references.iter().map(|r| r.to_string()).collect(),
            label: request.label,
            message: request.message,
            memo: request.memo,
        }
    }
}

pub fn link_from_request(request: SolanaPayRequestFFI) -> Result<String> {
    Ok(Url::from(SolanaPayRequest::from(request)).to_string())
}

pub fn request_from_url(url: String) -> Result<SolanaPayRequestFFI> {
    Ok(SolanaPayRequestFFI::from(
        SolanaPayRequest::try_from(&url).map_err(|e| anyhow::anyhow!(e.to_string()))?,
    ))
}

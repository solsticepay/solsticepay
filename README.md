# Solstice Pay

Social payments that revolve around _you_.

Send, receive, and share with those who matter most in your life.

More info is available on [the official website](https://solsticepay.com).

## Build instructions

### Linux development setup

Install the following dependencies:
 - `flutter`
 - `cmake`
 - `ninja-build`
 - `clang`
 - `pkgconf`
 - `libgtk-3-dev`
 - `libclang-dev`
 - `llvm`
 - `rustup`

Install the following Rust binary dependencies using `cargo-install`:
 - `cbindgen`
 - `cargo-ndk`
 - `just`
 - `flutter_rust_bridge_codegen`

If building for a desktop target, be sure to enable support using `flutter config --enable-linux-desktop`.

See the [`Dockerfile`](/Dockerfile) and [`.gitlab-ci.yml`](/.gitlab-ci.yml) for an example setup.

### macOS development setup

First, install Xcode and ensure that `Preferences > Location > Command Line Tools` is enabled

Then, in addition to the above dependencies for building on Linux:

```
cargo install cargo-lipo
rustup target add aarch64-apple-ios
rustup target add x86_64-apple-ios
cargo install cbindgen

# For iOS development:
brew install cocoapods

# For ASM 
mkdir homebrew
curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C homebrew
sudo mv homebrew /usr/local
arch -x86_64 /usr/local/homebrew/bin/brew install llvm
```

If building for a desktop target, be sure to enable support using `flutter config --enable-macos-desktop`.

### Setup for Android builds

In addition to the above, you'll need to install `android-sdk` and add the relevant Rust compilation targets:

```
rustup target add aarch64-linux-android armv7-linux-androideabi i686-linux-android x86_64-linux-android
```

Finally, be sure to add `ANDROID_NDK=/opt/android-ndk` (or the actual path on your machine) to `~/.gradle/gradle.properties.

### Building

To build and run, use the following command:

```
flutter run
```

More information about selecting a compilation target and runner device can be found in the `flutter` command line tool's documentation.

If you make any changes to the Rust bindings in the `native` directory, you may also need to run the `just` command to regenerate the bindings.

## License

Solstice Pay is available under the GPLv3.
